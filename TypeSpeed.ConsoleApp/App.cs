﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using TypeSpeed.ConsoleApp.Application.Presenters;

namespace TypeSpeed.ConsoleApp
{
    public class App : IHostedService, IDisposable
    {
        private readonly GamePresenter _gamePresenter;
        
        public App(GamePresenter gamePresenter)
        {
            _gamePresenter = gamePresenter;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _gamePresenter.Start();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
        }
    }
}