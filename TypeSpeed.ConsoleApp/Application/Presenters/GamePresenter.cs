﻿using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using TypeSpeed.ConsoleApp.Application.ViewModels;
using TypeSpeed.ConsoleApp.Application.Views;
using TypeSpeed.ConsoleApp.SystemWide.Extensions;
using TypeSpeed.GameLogic.TextProvider;
using TypeSpeed.GameLogic.TimeTracker;
using TypeSpeed.GameLogic.TypingAnalyzer;
using TypeSpeed.GameLogic.WordIterator;

namespace TypeSpeed.ConsoleApp.Application.Presenters
{
    public class GamePresenter
    {
        private readonly ITextProvider _textProvider;
        private readonly IGameView _gameView;
        private readonly ITimeTracker _timeTracker;
        private readonly ITypingAnalyzer _typingAnalyzer;

        public GamePresenter(
            IGameView gameView, 
            ITimeTracker timeTracker, 
            ITypingAnalyzer typingAnalyzer, 
            ITextProvider textProvider)
        {
            _gameView = gameView;
            _timeTracker = timeTracker;
            _typingAnalyzer = typingAnalyzer;
            _textProvider = textProvider;
            _gameView.GamePresenter = this;
        }
          
        public IWordIterator WordIterator { get; set; }
        private string TextToType { get; set; }
        private string CurrentTypedText { get; set; }

        public async Task Start()
        {
            TextToType = await _textProvider.Get();

            if (TextToType == string.Empty)
            {
                return;
            }
            
            WordIterator = new WordIterator(TextToType);
            
            _gameView.ShowStartScreen(TextToType);
        }

        public void StartStopwatch()
        {
            _timeTracker.Start();
        }

        public void StartTypingNext()
        {
            if (WordIterator.MoveNext())
            {
                _gameView.ShowStartedGameScreen(MapStartedGameViewModel());
            }

            EndGame();
        }

        public void ProcessTypedWord(string input)
        {
            CurrentTypedText += $"{input}" + " ";
            _typingAnalyzer.Analyze(WordIterator.Current(), input);
            
            StartTypingNext();
        }

        private void EndGame()
        {
            var elapsed = _timeTracker.End();
            var cps = _typingAnalyzer.CorrectTyped.SafeDivision(elapsed);
            
            var results = new EndedGameViewModel
            {
                SecondsElapsed = elapsed,
                TotalTyped = _typingAnalyzer.TypedInTotal,
                TypedText = CurrentTypedText,
                CharsPerSecond = cps,
                AverageCorrectTypedChars = _typingAnalyzer.GetAveragePercentage
            };
            _gameView.ShowEndedGameScreen(results);
        }

        private StartedGameViewModel MapStartedGameViewModel()
        {
            var currentElapsed = _timeTracker.GetCurrentElapsed();
            var cps = _typingAnalyzer.CorrectTyped.SafeDivision(currentElapsed);
            
            return new StartedGameViewModel
            {
                SecondsElapsed = currentElapsed,
                TypedText = CurrentTypedText,
                CurrentWordToType = WordIterator.Current(),
                CharsPerSecond = cps
            };
        }
    }
}