﻿namespace TypeSpeed.ConsoleApp.Application.ViewModels
{
    public class BaseGameViewModel
    {
        public string TypedText { get; set; }
        
        public int SecondsElapsed { get; set; }
        
        public decimal CharsPerSecond { get; set; }
    }
}