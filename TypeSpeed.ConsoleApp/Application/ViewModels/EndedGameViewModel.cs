﻿namespace TypeSpeed.ConsoleApp.Application.ViewModels
{
    public class EndedGameViewModel : BaseGameViewModel
    {
        public int TotalTyped { get; set; }
        public decimal AverageCorrectTypedChars { get; set; }
    }
}