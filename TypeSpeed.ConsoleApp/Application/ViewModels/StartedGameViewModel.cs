﻿namespace TypeSpeed.ConsoleApp.Application.ViewModels
{
    public class StartedGameViewModel : BaseGameViewModel
    {
        public string CurrentWordToType { get; set; }
    }
}