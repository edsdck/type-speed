﻿using System;
using System.Text;
using TypeSpeed.ConsoleApp.Application.Presenters;
using TypeSpeed.ConsoleApp.Application.ViewModels;
using TypeSpeed.ConsoleApp.SystemWide.ConsoleIO;
using TypeSpeed.ConsoleApp.SystemWide.Extensions;

namespace TypeSpeed.ConsoleApp.Application.Views
{
    public class GameView : IGameView
    {
        public GamePresenter GamePresenter { get; set; }
        
        public void ShowStartScreen(string text)
        {
            const string textToStart = "Press ENTER to begin the game";
            ColoredConsole.WriteClearColorLine(textToStart, ConsoleColor.DarkGreen);
            Console.WriteLine(text);

            ConsoleKeyPress.WaitFor(ConsoleKey.Enter);

            GamePresenter.StartStopwatch();
            GamePresenter.StartTypingNext();
        }

        public void ShowStartedGameScreen(StartedGameViewModel viewModel)
        {
            ShowGame(viewModel);

            var input = HandleInput(viewModel.CurrentWordToType).ToLower();
            
            GamePresenter.ProcessTypedWord(input); 
        }

        public void ShowEndedGameScreen(EndedGameViewModel viewModel)
        {
            Console.Clear();
            ColoredConsole.WriteColorLine("Your results", ConsoleColor.Red);

            var text = new StringBuilder(
                $"Characters per second {viewModel.CharsPerSecond:.00}\n" +
                $"You typed {viewModel.TotalTyped} characters in {viewModel.SecondsElapsed} seconds\n" +
                $"{viewModel.AverageCorrectTypedChars:0}% of typed characters were correct");

            Console.WriteLine(text);
        }

        private static void ShowGame(StartedGameViewModel viewModel)
        {
            var formattedText = $"Elapsed {viewModel.SecondsElapsed}s | " +
                                $"CPS {viewModel.CharsPerSecond:.00}";
            
            Console.Clear();
            Console.WriteLine(viewModel.TypedText);
            ColoredConsole.WriteColorLine(formattedText, ConsoleColor.Red);
            ColoredConsole.WriteColorLine(viewModel.CurrentWordToType, ConsoleColor.DarkGreen);
        }

        private static string HandleInput(string wordToType)
        {
            var input = string.Empty;
            do
            {
                var key = Console.ReadKey();
                input = HandleKeyOnInput(key, input);
            } 
            while (input.Length < wordToType.Length);
            
            return input;
        }

        private static string HandleKeyOnInput(ConsoleKeyInfo key, string currentInput)
        {
            if (key.Key is ConsoleKey.Backspace && currentInput.Length > 0)
            {
                currentInput = currentInput.Remove(currentInput.Length - 1);
            }
            else if (key.KeyChar.IsValid())
            {
                currentInput += key.KeyChar;
            }

            return currentInput;
        }
    }
}