﻿using TypeSpeed.ConsoleApp.Application.Presenters;
using TypeSpeed.ConsoleApp.Application.ViewModels;

namespace TypeSpeed.ConsoleApp.Application.Views
{
    public interface IGameView
    {
        GamePresenter GamePresenter { get; set; }
        
        void ShowStartScreen(string text);

        void ShowStartedGameScreen(StartedGameViewModel viewModel);

        void ShowEndedGameScreen(EndedGameViewModel viewModel);
    }
}