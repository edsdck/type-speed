﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TypeSpeed.ConsoleApp.Application.Presenters;
using TypeSpeed.ConsoleApp.Application.Views;
using TypeSpeed.GameLogic.Configuration;
using TypeSpeed.GameLogic.TextProvider;
using TypeSpeed.GameLogic.TimeTracker;
using TypeSpeed.GameLogic.TypingAnalyzer;

namespace TypeSpeed.ConsoleApp
{
    public static class Program
    {
        public static async Task Main()
        {
            await CreateHostBuilder().Build().RunAsync();
        }

        private static IHostBuilder CreateHostBuilder() =>
            Host.CreateDefaultBuilder()
                .ConfigureAppConfiguration((hostContext, services) =>
                {
                    services.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ApiSettings>(hostContext.Configuration.GetSection("apiSettings"));
                    
                    services.AddHttpClient();
                    
                    services.AddScoped<ITextProvider, TextProvider>();
                    services.AddScoped<ITimeTracker, TimeTracker>();
                    services.AddScoped<ITypingAnalyzer, TypingAnalyzer>();
                    
                    services.AddScoped<GamePresenter>();
                    services.AddScoped<IGameView, GameView>();
                    
                    services.AddSingleton<IHostedService, App>();
                })
                .ConfigureLogging((hostingContext, logging) => {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                });
    }
}