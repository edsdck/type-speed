﻿using System;

namespace TypeSpeed.ConsoleApp.SystemWide.ConsoleIO
{
    public static class ColoredConsole
    {
        public static void WriteClearColorLine(string message, ConsoleColor color)
        {
            Console.Clear();
            WriteColorLine(message, color);
        }
        
        public static void WriteColorLine(string message, ConsoleColor color)
        {
            Console.BackgroundColor = color;
            Console.WriteLine(message);
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}