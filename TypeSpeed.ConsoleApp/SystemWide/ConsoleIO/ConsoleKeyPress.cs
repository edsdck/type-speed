﻿using System;

namespace TypeSpeed.ConsoleApp.SystemWide.ConsoleIO
{
    public static class ConsoleKeyPress
    {
        public static void WaitFor(ConsoleKey key)
        {
            while (Console.ReadKey().Key != key) {}
        }
    }
}