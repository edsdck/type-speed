﻿namespace TypeSpeed.ConsoleApp.SystemWide.Extensions
{
    public static class NumericExtensions
    {
        public static decimal SafeDivision(this int numerator, int denominator)
        {
            return denominator == 0 ? 0 : (decimal) numerator / denominator;
        }
    }
}