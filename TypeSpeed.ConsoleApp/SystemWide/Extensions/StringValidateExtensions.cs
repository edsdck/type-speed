﻿using System.Text.RegularExpressions;

namespace TypeSpeed.ConsoleApp.SystemWide.Extensions
{
    public static class StringValidateExtensions
    {
        public static bool IsValid(this char key)
        {
            return Regex.IsMatch(key.ToString(), @"^[a-zA-Z-,.']+$");
        }
    }
}