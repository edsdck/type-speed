﻿namespace TypeSpeed.GameLogic.Configuration
{
    public class ApiSettings
    {
        public string ApiUrl { get; set; }
    }
}