﻿using System.Threading.Tasks;

namespace TypeSpeed.GameLogic.TextProvider
{
    public interface ITextProvider
    {
        Task<string> Get();
    }
}