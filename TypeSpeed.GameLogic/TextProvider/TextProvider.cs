﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TypeSpeed.GameLogic.Configuration;

namespace TypeSpeed.GameLogic.TextProvider
{
    public class TextProvider : ITextProvider
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ApiSettings _settings;
        
        public TextProvider(IHttpClientFactory httpClientFactory, IOptions<ApiSettings> settings)
        {
            _httpClientFactory = httpClientFactory;
            _settings = settings.Value;
        }
        
        public async Task<string> Get()
        {
            using var client = _httpClientFactory.CreateClient();
            
            using var response = await client.GetAsync(_settings.ApiUrl);
            response.EnsureSuccessStatusCode();

            await using var responseStream = await response.Content.ReadAsStreamAsync();
            var deserialized = await JsonSerializer.DeserializeAsync<IList<string>>(responseStream);

            return deserialized.First().ToLower();
        }
    }
}