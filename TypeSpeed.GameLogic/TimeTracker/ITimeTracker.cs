﻿namespace TypeSpeed.GameLogic.TimeTracker
{
    public interface ITimeTracker
    {
        void Start();
        int End();

        int GetCurrentElapsed();
    }
}