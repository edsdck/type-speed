﻿using System.Diagnostics;

namespace TypeSpeed.GameLogic.TimeTracker
{
    public class TimeTracker : ITimeTracker
    {
        private readonly Stopwatch _stopwatch;

        public TimeTracker()
        {
            _stopwatch = new Stopwatch();
        }
        
        public void Start()
        {
            _stopwatch.Reset();
            _stopwatch.Start();
        }

        public int End()
        {
            _stopwatch.Stop();
            
            return (int) _stopwatch.Elapsed.TotalSeconds;
        }

        public int GetCurrentElapsed()
        {
            return (int) _stopwatch.Elapsed.TotalSeconds;
        }
    }
}