﻿namespace TypeSpeed.GameLogic.TypingAnalyzer
{
    public interface ITypingAnalyzer
    {
        int Analyze(string expected, string actual);

        decimal GetAveragePercentage { get; }
        
        int CorrectTyped { get; }
        int TypedInTotal { get; }
    }
}