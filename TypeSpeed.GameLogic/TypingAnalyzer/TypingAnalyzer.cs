﻿using System.Linq;

namespace TypeSpeed.GameLogic.TypingAnalyzer
{
    public class TypingAnalyzer : ITypingAnalyzer
    {
        public int CorrectTyped { get; private set; }
        
        public int TypedInTotal { get; private set; }
        
        public int Analyze(string expected, string actual)
        {
            TypedInTotal += actual.Length;
            
            CorrectTyped += actual
                .Zip(expected)
                .Count(x => x.First == x.Second);

            return CorrectTyped;
        }

        public decimal GetAveragePercentage => (decimal) CorrectTyped / TypedInTotal * 100;
    }
}