﻿namespace TypeSpeed.GameLogic.WordIterator
{
    public interface IWordIterator
    {
        string Current();

        bool MoveNext();
    }
}