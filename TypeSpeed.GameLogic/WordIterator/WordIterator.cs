﻿using System;

namespace TypeSpeed.GameLogic.WordIterator
{
    public class WordIterator : IWordIterator
    {
        private readonly string[] _words;
        private int _position = -1;
        private readonly Random _random = new Random();
        
        public WordIterator(string text)
        {
            _words = text.ToLower().Split(' ');
        }
        
        public string Current()
        {
            return _words[_position];
        }
        
        public bool MoveNext()
        {
            var updatedPosition = _position + 1;
            if (updatedPosition >= 0 && updatedPosition < _words.Length)
            {
                _position = updatedPosition;
                TryReverseCurrent();
                
                return true;
            }
            
            return false;
        }

        private void TryReverseCurrent()
        {
            if (ShouldReverse())
            {
                var charArray = _words[_position].ToCharArray();
                Array.Reverse(charArray);
                
                _words[_position] = new string(charArray);
            }
        }

        private bool ShouldReverse()
        {
            return _random.Next(1, 3) == 1;
        }
    }
}