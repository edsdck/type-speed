﻿using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using TypeSpeed.ConsoleApp.Application.Presenters;
using TypeSpeed.ConsoleApp.Application.ViewModels;
using TypeSpeed.ConsoleApp.Application.Views;
using TypeSpeed.GameLogic.TextProvider;
using TypeSpeed.GameLogic.TimeTracker;
using TypeSpeed.GameLogic.TypingAnalyzer;
using TypeSpeed.GameLogic.WordIterator;

namespace TypeSpeed.UnitTests
{
    [TestFixture]
    public class GamePresenterTests
    {
        private GamePresenter _sut;
        private Mock<ITextProvider> _modelMock; 
        private Mock<IGameView> _viewMock; 
        private Mock<ITimeTracker> _timeTrackerMock; 
        private Mock<ITypingAnalyzer> _typingAnalyzerMock; 
        
        [SetUp]
        public void Setup()
        {
            _modelMock = new Mock<ITextProvider>();
            _viewMock = new Mock<IGameView>();
            _timeTrackerMock = new Mock<ITimeTracker>();
            _typingAnalyzerMock = new Mock<ITypingAnalyzer>();
            
            _sut = new GamePresenter(
                _viewMock.Object, 
                _timeTrackerMock.Object, 
                _typingAnalyzerMock.Object, 
                _modelMock.Object);
        }

        [Test]
        public async Task Start_ShouldShowStartScreen_WhenCalled()
        {
            const string text = "foo bar";
            _modelMock.Setup(m => m.Get()).ReturnsAsync(text);

            await _sut.Start();
            
            _viewMock.Verify(t => 
                t.ShowStartScreen(It.Is<string>(x => x == text)));
        }
        
        [Test]
        public async Task Start_ShouldEnd_WhenTextToWriteIsEmpty()
        {
            const string text = "";
            _modelMock.Setup(m => m.Get()).ReturnsAsync(text);

            await _sut.Start();
            
            _viewMock.Verify(t => t.ShowStartScreen(
                It.Is<string>(x => x == text)), Times.Never);
        }

        [Test]
        public void StartTypingNext_ShouldShowGameScreen_WhenNextWordExists()
        {
            var mockWordIterator = new Mock<IWordIterator>();
            _sut.WordIterator = mockWordIterator.Object;
            mockWordIterator.Setup(x => x.MoveNext()).Returns(true);
            mockWordIterator.Setup(x => x.Current()).Returns("foo");
            _timeTrackerMock.Setup(x => x.GetCurrentElapsed()).Returns(10);
            _typingAnalyzerMock.Setup(x => x.CorrectTyped).Returns(10);
            
            _sut.StartTypingNext();
            
            _viewMock.Verify(t => 
                t.ShowStartedGameScreen(It.Is<StartedGameViewModel>(x =>
                    x.SecondsElapsed == 10 && 
                    x.CurrentWordToType == "foo" &&
                    x.CharsPerSecond == 1)));
        }
    }
}